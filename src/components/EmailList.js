import React from "react";
import PropTypes from "prop-types";
import { List } from "immutable";
import ImmutablePropTypes from "react-immutable-proptypes";
import styled from "styled-components";

import { Link } from "react-router-dom";

import EmailListItem from "./EmailListItem";
import Colors from "../styles/colors";

export const Wrapper = styled.ul`
    list-style: none;
    padding: 0;
    margin: 0;

    li {
        border-bottom: 1px solid ${Colors.antiFlashWhite};

        &:last-child {
            border: 0;
        }
    }
`;

export const LinkWrapper = styled(Link)`
    text-decoration: inherit;
    color: inherit;
`;

const EmailList = ({ emails, openedUID, onDelete }) => (
    <Wrapper>
        {
            emails.map(email => (
                <li key={email.get("uid")}>
                    <LinkWrapper to={`/${email.get("uid")}`}>
                        <EmailListItem
                            active={email.get("uid") === openedUID}
                            email={email}
                            onDelete={(evt) => {
                                evt.preventDefault();
                                onDelete(email.get("uid"));
                            }}
                        />
                    </LinkWrapper>
                </li>
            ))
        }
    </Wrapper>
);

EmailList.propTypes = {
    emails:    ImmutablePropTypes.listOf(ImmutablePropTypes.mapContains({ uid: PropTypes.string.isRequired })),
    onDelete:  PropTypes.func.isRequired,
    openedUID: PropTypes.string,
};

EmailList.defaultProps = {
    emails:    List(),
    openedUID: "",
};

export default EmailList;
