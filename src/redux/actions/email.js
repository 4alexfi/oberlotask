export const EMAILS_ADD = "[Email] Add emails";
export const EMAIL_OPEN = "[Email] Open";
export const EMAIL_MOVE_TO_TRASH = "[Email] Move to trash";
export const EMPTY_TRASH = "[Email] Empty trash";
export const RESTORE_TRASH = "[Email] Restore trash";

export const emailsAdd = emails => ({
    emails,
    type: EMAILS_ADD,
});

export const emailOpen = uid => ({
    uid,
    type: EMAIL_OPEN,
});

export const emailMoveToTrash = uid => ({
    uid,
    type: EMAIL_MOVE_TO_TRASH,
});

export const emptyTrash = () => ({
    type: EMPTY_TRASH,
});

export const restoreTrash = () => ({
    type: RESTORE_TRASH,
});
