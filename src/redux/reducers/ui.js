import { fromJS } from "immutable";

import {
    UPLOAD_DATA_FILE,
    UPLOAD_DATA_FILE_SUCCESS,
    UPLOAD_DATA_FILE_FAILURE,
} from "../actions/ui";

export const InitialState = fromJS({
    uploadingDataFile: false,
    uploadError:       "",
});

const emailReducer = (state = InitialState, action) => {
    switch (action.type) {
        case UPLOAD_DATA_FILE:
            return state.set("uploadingDataFile", true).set("uploadError", "");

        case UPLOAD_DATA_FILE_SUCCESS:
            return state.set("uploadingDataFile", false).set("uploadError", "");

        case UPLOAD_DATA_FILE_FAILURE:
            return state.set("uploadingDataFile", false).set("uploadError", action.error);

        default:
            return state;
    }
};

export default emailReducer;
