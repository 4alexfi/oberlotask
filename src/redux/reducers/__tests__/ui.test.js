import { fromJS } from "immutable";

import reducer, { InitialState } from "../ui";
import {
    UPLOAD_DATA_FILE,
    UPLOAD_DATA_FILE_SUCCESS,
    UPLOAD_DATA_FILE_FAILURE,
} from "../../actions/ui";

describe("ui reducer", () => {
    it("should return the initial state", () => {
        expect(reducer(undefined, {})).toEqual(InitialState);
    });

    it("should handle successful data upload", () => {
        expect(reducer(InitialState, {
            type: UPLOAD_DATA_FILE,
            file: "file",
        })).toEqual(fromJS({
            uploadingDataFile: true,
            uploadError:       "",
        }));

        expect(reducer(
            fromJS({
                uploadingDataFile: true,
                uploadError:       "",
            }),
            {
                type: UPLOAD_DATA_FILE_SUCCESS,
            },
        )).toEqual(fromJS({
            uploadingDataFile: false,
            uploadError:       "",
        }));
    });

    it("should handle failed data upload", () => {
        expect(reducer(undefined, {
            type: UPLOAD_DATA_FILE,
            file: "file",
        })).toEqual(fromJS({
            uploadingDataFile: true,
            uploadError:       "",
        }));

        expect(reducer(
            fromJS({
                uploadingDataFile: true,
                uploadError:       "",
            }),
            {
                type:  UPLOAD_DATA_FILE_FAILURE,
                error: "Some error",
            },
        )).toEqual(fromJS({
            uploadingDataFile: false,
            uploadError:       "Some error",
        }));
    });
});
