export const UPLOAD_DATA_FILE = "[ui] Upload data file";
export const UPLOAD_DATA_FILE_SUCCESS = "[ui] Succesful data file upload";
export const UPLOAD_DATA_FILE_FAILURE = "[ui] Failed data file upload";

export const uploadDataFile = file => ({
    file,
    type: UPLOAD_DATA_FILE,
});

export const uploadDataFileSuccess = emails => ({
    emails,
    type: UPLOAD_DATA_FILE_SUCCESS,
});

export const uploadDataFileFailure = error => ({
    error,
    type: UPLOAD_DATA_FILE_FAILURE,
});
