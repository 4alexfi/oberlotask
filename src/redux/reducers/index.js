import { combineReducers } from "redux";

import email from "./email";
import ui from "./ui";

export default combineReducers({
    email,
    ui,
});
