export default {
    antiFlashWhite:  "#F2F2F2",
    brightNavyBlue:  "#0D70E2",
    aliceBlue:       "#F3F8FD",
    spanishGray:     "#999999",
    mediumVermilion: "#E2553D",
    white:           "#FFFFFF",
    black:           "#000000",
    blackOverlay:    "rgba(0, 0, 0, 0.7)",
};
