import styled, { css } from "styled-components";
import Colors from "../styles/colors";

export const GrayLabel = styled.label`
    display: block;
    font-size: 12px;
    color: ${Colors.spanishGray};
`;

export const Paragraph = styled.p`
    font-size: 13px;
    font-weight: 400;
    line-height: 1.6;
    margin: 0;
`;

export const ellipsis = css`
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
`;

export const TextButton = styled.button`
    background: none;
    border: none;
    font-weight: 600;
    text-decoration: underline;
    cursor: pointer;
    outline: none;
    border: 0;
`;
