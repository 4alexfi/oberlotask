import React from "react";
import { Provider } from "react-redux";
import { Router, Route } from "react-router-dom";
import styled from "styled-components";

import { createBrowserHistory } from "history";

import Sidebar from "./containers/Sidebar";
import Content from "./containers/Content";
import store from "./redux/store";

const Wrapper = styled.div`
    height: 100%;
`;

const App = () => (
    <Provider store={store}>
        <Router history={createBrowserHistory()}>
            <Wrapper>
                <Route exact path="/:uid?" component={Sidebar} />
                <Route exact path="/:uid" component={Content} />
            </Wrapper>
        </Router>
    </Provider>
);

export default App;
