import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import ImmutablePropTypes from "react-immutable-proptypes";
import PropTypes from "prop-types";
import styled from "styled-components";

import { getEmail } from "../redux/selectors";
import EmailInstance from "../components/EmailInstance";
import { emailOpen } from "../redux/actions/email";
import Media from "../styles/media";
import Colors from "../styles/colors";
import { GrayLabel } from "../styles/typography";

const Wrapper = styled.div`
    width: 66.66%;
    display: inline-block;
    vertical-align: top;

    ${Media.md`width: 60%`};
    ${Media.xs`width: 100%`};
`;

const EmptyState = GrayLabel.extend`
    display: block;
    text-align: center;
    padding-top: 50px;
`;

const CloseLink = styled(Link)`
    position: relative;
    float: right;
    color: ${Colors.black};
    font-size: 20px;
    padding: 20px;
    text-decoration: none;
    z-index: 1;
`;

class Content extends React.PureComponent {
    componentDidMount() {
        this.markEmailOpened();
    }

    componentDidUpdate() {
        this.markEmailOpened();
    }

    markEmailOpened() { // TODO: this logic would be moved to router middleware, and should be fired on route event.
        const { markOpened, email } = this.props;
        markOpened(email.get("uid"));
    }

    render() {
        const { email } = this.props;
        return (
            <Wrapper>
                <CloseLink to="/">&#x2715;</CloseLink>
                {
                    email.isEmpty() ?
                        <EmptyState>Email not found</EmptyState> :
                        <EmailInstance email={email} />
                }
            </Wrapper>
        );
    }
}

Content.propTypes = {
    email:      ImmutablePropTypes.mapContains({ uid: PropTypes.string }).isRequired,
    markOpened: PropTypes.func.isRequired,
};

const mapProps = (state, { match: { params: { uid } } }) => ({
    email: getEmail(state, uid),
});

const mapDispatcher = dispatch => ({
    markOpened: (uid) => {
        dispatch(emailOpen(uid));
    },
});

export default connect(mapProps, mapDispatcher)(Content);
