import { css } from "styled-components";

const sizes = {
    hd: 1280,
    md: 1024,
    xs: 600,
};

export default Object.keys(sizes).reduce((acc, label) => {
    acc[label] = (...args) => css`
    @media (max-width: ${sizes[label] / 16}em) {
      ${css(...args)}
    }
  `;

    return acc;
}, {});
