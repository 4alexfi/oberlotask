import { applyMiddleware, createStore, compose } from "redux";

import reducers from "./reducers";
import { uiMiddleware } from "./middleware/ui";

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducers,
    composeEnhancers(applyMiddleware(...uiMiddleware)),
);

export default store;
