import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

import Colors from "../styles/colors";
import { TextButton } from "../styles/typography";

const Wrapper = styled.div`
    position: sticky;
    bottom: 0;
    left: 0;
    color: ${Colors.white};
    width: 100%;
    padding: 20px 0;
`;

const Content = styled.div`
    border-radius: 5px;
    padding: 10px 20px;
    background: ${Colors.blackOverlay};
    margin: 0 20px;
`;

const UndoButton = TextButton.extend`
    color: ${Colors.white};
    float: right;
    cursor: pointer;
`;

const Message = styled.label`
    display: inline-block;
    font-size: 14px;
`;

const UndoDeletePopup = ({ number, onUndo }) => (
    <Wrapper>
        <Content>
            <UndoButton onClick={(evt) => {
                evt.preventDefault();
                onUndo();
            }}
            >
                UNDO
            </UndoButton>
            <Message>{ number === 1 ? `${number} email deleted` : `${number} emails deleted` }</Message>
        </Content>
    </Wrapper>
);

UndoDeletePopup.propTypes = {
    number: PropTypes.number.isRequired,
    onUndo: PropTypes.func.isRequired,
};

export default UndoDeletePopup;
