import {
    UPLOAD_DATA_FILE,
    UPLOAD_DATA_FILE_SUCCESS,
    uploadDataFileSuccess,
    uploadDataFileFailure,
} from "../actions/ui";

import { EMAIL_MOVE_TO_TRASH, RESTORE_TRASH, emptyTrash, emailsAdd } from "../actions/email";

const DELETE_EMAIL_TIMEOUT = 3000;

let deleteTimeoutHandle;

export const deleteTrashedEmailFlow = ({ dispatch }) => next => (action) => {
    next(action);

    if (action.type === EMAIL_MOVE_TO_TRASH) {
        clearTimeout(deleteTimeoutHandle);
        deleteTimeoutHandle = setTimeout(() => {
            dispatch(emptyTrash());
        }, DELETE_EMAIL_TIMEOUT);
    }
};

export const clearDeleteTimeout = () => next => (action) => {
    next(action);

    if (action.type === RESTORE_TRASH) {
        clearTimeout(deleteTimeoutHandle);
    }
};

export const uploadDataFileFlow = ({ dispatch }) => next => (action) => {
    next(action);

    if (action.type === UPLOAD_DATA_FILE) {
        const reader = new FileReader();
        reader.onload = () => {
            try {
                const { messages = [] } = JSON.parse(reader.result);
                dispatch(uploadDataFileSuccess(messages));
            } catch (e) {
                dispatch(uploadDataFileFailure(e.message));
            }
        };
        reader.readAsText(action.file);
    }
};

export const uploadDataFileSuccessFlow = ({ dispatch }) => next => (action) => {
    next(action);

    if (action.type === UPLOAD_DATA_FILE_SUCCESS) {
        dispatch(emailsAdd(action.emails));
    }
};

export const uiMiddleware = [uploadDataFileFlow, uploadDataFileSuccessFlow, deleteTrashedEmailFlow];
