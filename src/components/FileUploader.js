import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import styled from "styled-components";

import { getDataFileUploadError } from "../redux/selectors";
import { uploadDataFile } from "../redux/actions/ui";
import Colors from "../styles/colors";
import { GrayLabel } from "../styles/typography";

const Error = styled.div`
    text-align: center;
    font-size: 12px;
    color: ${Colors.mediumVermilion};
    line-height: 30px;
`;

const EmptyState = GrayLabel.extend`
    display: block;
    text-align: center;
    padding-top: 50px;
`;

const FileUploader = ({ error, handleFileUpload }) => (
    <EmptyState>
        <input
            type="file"
            onChange={(event) => {
                if (typeof event.target.files === "object") {
                    handleFileUpload(event.target.files[0]);
                }
            }}
        />
        { error && <Error>{ error }</Error> }
    </EmptyState>
);

FileUploader.propTypes = {
    error:            PropTypes.string,
    handleFileUpload: PropTypes.func.isRequired,
};

FileUploader.defaultProps = {
    error: "",
};

const mapProps = state => ({
    error: getDataFileUploadError(state),
});

const mapDispatcher = dispatch => ({
    handleFileUpload: (file) => {
        dispatch(uploadDataFile(file));
    },
});

export default connect(mapProps, mapDispatcher)(FileUploader);
