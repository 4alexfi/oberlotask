import { fromJS } from "immutable";

import reducer, { InitialState } from "../email";
import {
    EMAILS_ADD,
    EMAIL_OPEN,
    EMAIL_MOVE_TO_TRASH,
    EMPTY_TRASH,
    RESTORE_TRASH,
} from "../../actions/email";

describe("ui reducer", () => {
    it("should return the initial state", () => {
        expect(reducer(undefined, {})).toEqual(InitialState);
    });

    it("should add emails", () => {
        expect(reducer(InitialState, {
            type:   EMAILS_ADD,
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
        })).toEqual(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
            trash:  [],
        }));
    });

    it("should open email", () => {
        expect(reducer(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
            trash:  [],
        }), {
            type:   EMAIL_OPEN,
            uid:    "21",
        })).toEqual(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                    opened:    true,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
            trash:  [],
        }));
    });

    it("should move email to trash", () => {
        expect(reducer(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
            trash:  [],
        }), {
            type:   EMAIL_MOVE_TO_TRASH,
            uid:    "22",
        })).toEqual(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
            ],
            trash:  [
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },

            ],
        }));
    });


    it("should empty trash", () => {
        expect(reducer(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
            ],
            trash:  [
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
        }), {
            type:   EMPTY_TRASH,
            uid:    "22",
        })).toEqual(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
            ],
            trash:  [],
        }));
    });

    it("should restore trash", () => {
        expect(reducer(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
            ],
            trash:  [
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
        }), {
            type:   RESTORE_TRASH,
        })).toEqual(fromJS({
            emails: [
                {
                    uid:       "21",
                    sender:    "Ernest Hemingway",
                    subject:   "animals",
                    message:   "message",
                    time_sent: 1459239867,
                },
                {
                    uid:       "22",
                    sender:    "Stephen King",
                    subject:   "adoration",
                    message:   "message",
                    time_sent: 1459248747,
                },
            ],
            trash:  [],
        }));
    });
});
