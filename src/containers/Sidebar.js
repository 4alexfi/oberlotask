import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import styled from "styled-components";

import EmailList from "../components/EmailList";
import FileUploader from "../components/FileUploader";
import UndoDeletePopup from "../components/UndoDeletePopup";

import { getSortedEmails, getTrash, getDataFileUploadError } from "../redux/selectors";
import { emailMoveToTrash, restoreTrash } from "../redux/actions/email";
import Colors from "../styles/colors";
import Media from "../styles/media";
import { GrayLabel } from "../styles/typography";

const Wrapper = styled.div`
    position: relative;
    display: inline-block;
    width: 33.33%;
    box-shadow: 1px 0px 0px ${Colors.antiFlashWhite};
    height: 100%;
    vertical-align: top;
    overflow-y: scroll;

    ${Media.md`width: 40%`};
    ${({ forceMobileVisible }) => (forceMobileVisible ? Media.xs`width: 100%` : Media.xs`display: none;`)}}
`;

const EmptyState = GrayLabel.extend`
    display: block;
    text-align: center;
    padding-top: 50px;
`;

const Sidebar = ({
    isRootPath, openedUID, emails, trash, handleDelete, handleRestoreTrash,
}) => (
    <Wrapper forceMobileVisible={isRootPath}>
        {
            emails.isEmpty() ?
                <EmptyState>
                    No emails
                    <FileUploader />
                </EmptyState> :
                <EmailList
                    emails={emails}
                    onDelete={handleDelete}
                    openedUID={openedUID}
                />
        }
        {
            !trash.isEmpty() &&
            <UndoDeletePopup number={trash.size} onUndo={handleRestoreTrash} />
        }
    </Wrapper>
);

Sidebar.propTypes = {
    emails:             ImmutablePropTypes.listOf(ImmutablePropTypes.map).isRequired,
    trash:              ImmutablePropTypes.listOf(ImmutablePropTypes.map).isRequired,
    openedUID:          PropTypes.string,
    isRootPath:         PropTypes.bool.isRequired,
    handleDelete:       PropTypes.func.isRequired,
    handleRestoreTrash: PropTypes.func.isRequired,
};

Sidebar.defaultProps = {
    openedUID: "",
};

const mapProps = (state, { match: { params: { uid = "" } } }) => ({
    emails:      getSortedEmails(state),
    openedUID:   uid,
    isRootPath:  !uid,
    uploadError: getDataFileUploadError(state),
    trash:       getTrash(state),
});

const mapDispatcher = dispatch => ({
    handleDelete: (uid) => {
        dispatch(emailMoveToTrash(uid));
    },
    handleRestoreTrash: () => {
        dispatch(restoreTrash());
    },
});

export default connect(mapProps, mapDispatcher)(Sidebar);
