import React from "react";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import styled from "styled-components";

import { ellipsis, Paragraph, GrayLabel } from "../styles/typography";

const Subject = styled.h1`
    font-size: 24px;
    font-weight: 600;
    margin-top: 20px;
    margin-bottom: 10px;
    ${ellipsis}
`;

const Message = Paragraph.extend`
    font-size: 14px;
    margin: 20px 0;
`;

const Wrapper = styled.div`
    position: relative;
    padding: 0px 40px;
`;

const EmailInstance = ({ email }) => (
    <Wrapper>
        <Subject>{ email.get("subject") }</Subject>
        <GrayLabel>
            {
                `by ${email.get("sender")}`
            }
        </GrayLabel>
        <Message>{ email.get("message") }</Message>
    </Wrapper>
);

EmailInstance.propTypes = {
    email:  ImmutablePropTypes.mapContains({
        subject:   PropTypes.string,
        sender:    PropTypes.string.isRequired,
        message:   PropTypes.string.isRequired,
        time_sent: PropTypes.number.isRequired,
    }).isRequired,
};

export default EmailInstance;
