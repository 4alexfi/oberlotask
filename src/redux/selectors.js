import { List, Map } from "immutable";
import { createSelector } from "reselect";

export const getEmails = ({ email }) => email.get("emails", List());
export const getSortedEmails = createSelector(
    getEmails,
    emails => emails.sort((a, b) => a.get("time_sent") < b.get("time_sent")),
);
export const getEmail = createSelector(
    getEmails,
    (state, uid) => uid,
    (emails, uid) => emails.filter(email => email.get("uid") === uid).first() || Map(),
);

export const getTrash = ({ email }) => email.get("trash", List());
export const getDataFileUploadError = ({ ui }) => ui.get("uploadError");
