import { List, fromJS } from "immutable";

import { EMPTY_TRASH, RESTORE_TRASH, EMAILS_ADD, EMAIL_MOVE_TO_TRASH, EMAIL_OPEN } from "../actions/email";

export const InitialState = fromJS({
    emails:      [],
    trash:       [],
});

const emailReducer = (state = InitialState, action) => {
    const emails = state.get("emails", List());
    const trash = state.get("trash", List());

    switch (action.type) {
        case EMAILS_ADD:
            return state.set("emails", emails.concat(fromJS(action.emails)));

        case EMAIL_OPEN:
            return state.set(
                "emails",
                emails.map(email => (email.get("uid") === action.uid ? email.set("opened", true) : email)),
            );

        case EMAIL_MOVE_TO_TRASH:
            return state
                .set(
                    "trash",
                    trash.concat(emails.filter(email => email.get("uid") === action.uid)),
                )
                .set(
                    "emails",
                    emails.filter(email => email.get("uid") !== action.uid),
                );

        case EMPTY_TRASH:
            return state.set("trash", List());

        case RESTORE_TRASH:
            return state.set("trash", List()).set("emails", emails.concat(trash));

        default:
            return state;
    }
};

export default emailReducer;
