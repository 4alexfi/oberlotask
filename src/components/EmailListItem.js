import React from "react";
import PropTypes from "prop-types";
import ImmutablePropTypes from "react-immutable-proptypes";
import styled, { css } from "styled-components";
import moment from "moment";

import Colors from "../styles/colors";
import { ellipsis, GrayLabel, TextButton } from "../styles/typography";

const DATE_FORMAT = "ddd DD MMMM, HH:MM";

const activeBackgroundStyle = css`
    ${({ active = false }) => active && `background-color: ${Colors.aliceBlue};`}
`;

const openedStyle = css`
    ${({ opened = false }) => !opened && `
        &:before {
            content: " ";
            position: absolute;
            top: 50%;
            transform: translateY(-50%);
            left: 15px;
            background-color: ${Colors.brightNavyBlue};
            width: 10px;
            height: 10px;
            border-radius: 5px;
        }
    `}
`;

export const Subject = styled.h5`
    font-size: 14px;
    font-weight: 600;
    margin: 0;
    margin-bottom: 10px;
    ${ellipsis}
`;

const DeleteButton = TextButton.extend`
    float: right;
    margin-right: -30px;
    display: none;
    color: ${Colors.brightNavyBlue};
    background: none;
`;

export const Wrapper = styled.div`
    position: relative;
    padding: 20px 40px;
    cursor: pointer;

    * {
        cursor: pointer;
    }

    ${activeBackgroundStyle}
    ${openedStyle}

    &:hover {
        ${DeleteButton} {
            display: block;
        }
    }
`;

const EmailListItem = ({
    active, email, onDelete, ...props
}) => (
    <Wrapper active={active} opened={email.get("opened")} {...props}>
        <DeleteButton onClick={onDelete}>Delete</DeleteButton>
        <Subject>{ email.get("subject") }</Subject>
        <GrayLabel>
            {
                `${moment(new Date(email.get("time_sent") * 1000)).format(DATE_FORMAT)} by ${email.get("sender")}`
            }
        </GrayLabel>
    </Wrapper>
);

EmailListItem.propTypes = {
    active: PropTypes.bool,
    email:  ImmutablePropTypes.mapContains({
        subject:   PropTypes.string,
        sender:    PropTypes.string.isRequired,
        time_sent: PropTypes.number.isRequired,
    }).isRequired,
    onDelete: PropTypes.func.isRequired,
};

EmailListItem.defaultProps = {
    active: false,
};

export default EmailListItem;
